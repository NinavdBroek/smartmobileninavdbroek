package com.example.ninavandenbroek.mapapp;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BeoordelingDao {

    @Query("SELECT * FROM beoordeling ")
    List<Beoordeling> getAll();

    @Query("SELECT * FROM beoordeling WHERE locatieId = :locatieId ")
    List<Beoordeling> loadOnLocation(int locatieId);

    @Insert
    void insertAll(Beoordeling... beoordelingen);

    @Delete
    void delete(Beoordeling beoordeling);

}
