package com.example.ninavandenbroek.mapapp;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.os.AsyncTask;
import android.widget.Toast;


public class MapsActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback {
    private final int MAX_PLACES = 20;
    static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;

    private PopupWindow popupWindow;
    private LayoutInflater layoutInflater;
    private LinearLayout relativeLayout;

    private GoogleMap mMap;
    private LocationListener locationListener;
    double latti = 0.0;
    double longi = 0.0;
    //    LatLng school = new LatLng(51.4727204 , 5.8068715 );
    AppDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "LocationDatabase").fallbackToDestructiveMigration().build();

        new AddLocationsTask().execute(db);
        new AddBeoordelingTask().execute(db);


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLocation();

        relativeLayout = findViewById(R.id.mapslayout);

        int a = 6 + 2;

    }


    void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

            if (location != null) {
                // locatie wordt opgevraagd
                latti = location.getLatitude();
                longi = location.getLongitude();

//                // Locatie wordt toegevoegd aan textviews onder kaartje
                TextView lattid = findViewById(R.id.latitude);
                lattid.setText("Latitude: " + latti);
                TextView longd = findViewById(R.id.longtitude);
                longd.setText("Latitude: " + longi);
            } else {
                TextView lattid = findViewById(R.id.latitude);
                lattid.setText("Not found ");
                TextView longd = findViewById(R.id.longtitude);
                longd.setText("Not found ");
            }
        }

    }


    protected void getPlacesOfzo() throws IOException {
        URL url = new URL("http://www.android.com/");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            readStream(in);
        } finally {
            urlConnection.disconnect();
        }
    }

    private void readStream(InputStream in) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMarkerClickListener(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }//

//        mMap.setMyLocationEnabled(true);
        UiSettings mapSettings = mMap.getUiSettings();
        mapSettings.setScrollGesturesEnabled(true);
        mapSettings.setZoomControlsEnabled(true);


        // Add a marker in Sydney and move the camera
        LatLng lcation = new LatLng(latti, longi);
        mMap.addMarker(new MarkerOptions().position(lcation).title("Your location")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(lcation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14.0f));

        List<Object> o = new ArrayList<Object>();
        o.add(mMap);
        o.add(db);
        new ShowLocationMap().execute(mMap, db);


//        Marker OpaOma = mMap.addMarker(new MarkerOptions().position(school).title("Opa en Oma")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));


    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        String naam = marker.getTitle();

        new ShowLocationInfo().execute(naam, db, this);

        return false;
    }

    public boolean displayToast(com.example.ninavandenbroek.mapapp.Location loc, Beoordeling ber) {

        TextView dynamicTextView = new TextView(this);
        dynamicTextView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        dynamicTextView.setText("     " + loc.getName());

        TextView beoordeling = new TextView(this);
        beoordeling.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        beoordeling.setText( "\n "+   "\n " +  ber.getNaam() +   "\n "+ "cijfer:  " + new Integer(ber.getCijfer()).toString()+ "\n "+ "Ervaring: " + ber.getErvaring( )+  "\n "+   "\n "+  "Plant om " + ber.getDatumPlanning() + "\n" + "weer te gaan."+  "\n "+   "\n "+"     Contact: " + ber.getNummer() );



        layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

        ViewGroup container = (ViewGroup) layoutInflater.inflate(R.layout.popup,null);


        popupWindow = new PopupWindow(container, 800, 800,true);
        container.addView(dynamicTextView);
        container.addView(beoordeling);
        dynamicTextView.setTextAppearance(getApplicationContext(), R.style.fontForNotificationLandingPage);
        beoordeling.setTextAppearance(getApplicationContext(), R.style.beoordeling);



        popupWindow.showAtLocation(relativeLayout, Gravity.NO_GRAVITY, 150, 600);

        container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

        Toast.makeText(getApplicationContext(), ber.getNaam() + loc.getId(), Toast.LENGTH_LONG).show();
        return true;
    }
}






