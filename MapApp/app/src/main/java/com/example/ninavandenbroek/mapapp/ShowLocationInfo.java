package com.example.ninavandenbroek.mapapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ShowLocationInfo extends AsyncTask<Object, Integer, Boolean> {
    String Naam;
    AppDatabase db;
    //    List<Location> list;
    com.example.ninavandenbroek.mapapp.Location loc;
    List<Beoordeling> ber;
    MapsActivity mapsscherm;
    Beoordeling beord;

    @Override
    protected Boolean doInBackground (Object... params ) {
        Object[] objs = params;

        Naam = (String) objs[0];
        db = (AppDatabase) objs[1];
        mapsscherm = (MapsActivity) objs[2];

        loc = db.locationDao().findByName(Naam);
        ber = db.beoordelingDao().loadOnLocation(loc.getId());


        return true;
    }

    @Override
    protected void onPostExecute(Boolean bool) {
        Iterator iterator = ber.iterator();
        while (iterator.hasNext()) {
            beord = (Beoordeling) iterator.next();
            mapsscherm.displayToast(loc, beord);
        }
    }
}