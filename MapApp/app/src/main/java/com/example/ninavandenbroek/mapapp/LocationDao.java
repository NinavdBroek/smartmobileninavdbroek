package com.example.ninavandenbroek.mapapp;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.ninavandenbroek.mapapp.Location;

import java.util.List;

@Dao
public interface LocationDao {
    @Query("SELECT * FROM location")
    List<Location> getAll();

    @Query("SELECT * FROM location WHERE id IN (:locationIds)")
     List<Location> loadAllByIds(int[] locationIds);

    @Query("SELECT * FROM location WHERE name LIKE :name LIMIT 1")
     Location findByName(String name);

    @Insert
     void insertAll(Location... locations);

    @Delete
    void delete(Location location);

}

