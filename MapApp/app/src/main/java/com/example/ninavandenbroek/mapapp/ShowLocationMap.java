package com.example.ninavandenbroek.mapapp;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ShowLocationMap extends AsyncTask<Object, Integer, Boolean> {
    GoogleMap map;
    AppDatabase db;
    List<Location> list;

    @Override
    protected Boolean doInBackground (Object... params ) {
        Object[] objs = params;

        map = (GoogleMap) objs[0];
        db = (AppDatabase) objs[1];

        list = db.locationDao().getAll();

        return true;
    }

    @Override
    protected void onPostExecute(Boolean bool) {

        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Location loc;
            loc = (Location) iterator.next();
            LatLng plaats = new LatLng(loc.getLatitude(), loc.getLongtitude());

            Marker pin = map.addMarker(new MarkerOptions().position(plaats).title(loc.getName())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        }
    }
}
