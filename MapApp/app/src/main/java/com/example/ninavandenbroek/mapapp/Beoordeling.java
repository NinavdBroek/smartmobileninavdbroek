package com.example.ninavandenbroek.mapapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class Beoordeling {

        @PrimaryKey
        private int id;

        @ColumnInfo(name = "locatieId")
        public int locatieId;

        @ColumnInfo(name = "Naam")
        public String Naam;

        @ColumnInfo(name = "Nummer")
        private String Nummer;

        @ColumnInfo(name = "Cijfer")
        private int Cijfer;

        @ColumnInfo(name = "Ervaring")
        private String Ervaring;

        @ColumnInfo(name = "DatumPlanning")
        private String DatumPlanning;

        public String getNaam(){
            return Naam;
        }

        public void setNaam(String Naam) {
            this.Naam = Naam;
        }

        public int getId(){
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLocatieId(){
            return locatieId;
        }

        public void setLocatieId(int locid) {
            this.locatieId = locid;
        }

        public String getNummer(){
            return Nummer;
        }

        public void setNummer(String nummer) {
            this.Nummer = nummer;
        }

        public int getCijfer(){
            return Cijfer;
        }

        public void setCijfer(int cijfer) {
            this.Cijfer = cijfer;
        }

        public String getErvaring(){
            return Ervaring;
        }

        public void setErvaring(String ervaring) {
            this.Ervaring = ervaring;
        }

        public String getDatumPlanning(){
            return DatumPlanning;
        }

        public void setDatumPlanning(String datumplan) {
            this.DatumPlanning = datumplan;
        }
}
